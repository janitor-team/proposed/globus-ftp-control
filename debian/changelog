globus-ftp-control (9.10-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 21:06:18 +0200

globus-ftp-control (9.10-1) unstable; urgency=medium

  * New GCT release v6.2.20220524
  * Drop patches included in the release

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 25 May 2022 22:13:12 +0200

globus-ftp-control (9.7-2) unstable; urgency=medium

  * Use sha256 hash when generating test certificates
  * Fix some compiler and doxygen warnings

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 12 May 2022 13:55:20 +0200

globus-ftp-control (9.7-1) unstable; urgency=medium

  * Typo fixes

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 27 Aug 2021 09:54:26 +0200

globus-ftp-control (9.6-1) unstable; urgency=medium

  * Minor fixes to makefiles (9.5)
  * Use -nameopt sep_multiline to derive certificate subject string (9.6)
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Mon, 14 Dec 2020 03:14:50 +0100

globus-ftp-control (9.4-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:29 +0200

globus-ftp-control (9.4-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:48 +0100

globus-ftp-control (9.3-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release (9.0)
  * Use 2048 bit RSA key for tests (9.1)
  * Merge GT6 update 8.4 into GCT (9.2)
  * Merge GT6 update 8.5 into GCT (9.3)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:31 +0200

globus-ftp-control (8.6-1) unstable; urgency=medium

  * GT6 update: Use 2048 bit keys to support openssl 1.1.1
  * Drop patch globus-ftp-control-2048-bits.patch (accepted upstream)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sat, 01 Sep 2018 18:42:13 +0200

globus-ftp-control (8.5-2) unstable; urgency=medium

  * Use 2048 bit RSA key for tests

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 26 Aug 2018 09:40:22 +0200

globus-ftp-control (8.5-1) unstable; urgency=medium

  * GT6 update: Force encryption on TLS control channel

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 17 Jul 2018 22:17:39 +0200

globus-ftp-control (8.4-1) unstable; urgency=medium

  * GT6 update: Check for missing signing policy req flag

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 28 Jun 2018 15:34:20 +0200

globus-ftp-control (8.3-1) unstable; urgency=medium

  * GT6 update: Default to host authz when using TLS control channel
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 08 Apr 2018 17:21:20 +0200

globus-ftp-control (8.2-1) unstable; urgency=medium

  * GT6 update: Fix leak

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 22 Oct 2017 19:42:11 +0200

globus-ftp-control (8.1-1) unstable; urgency=medium

  * GT6 update: Reading when EOF will result in callback indicating EOF instead
    of error

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 26 Sep 2017 11:37:09 +0200

globus-ftp-control (8.0-1) unstable; urgency=medium

  * GT6 update: Add function globus_ftp_control_use_tls() for TLS control
    channel

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 10 Sep 2017 09:58:51 +0200

globus-ftp-control (7.8-1) unstable; urgency=medium

  * GT6 update: Fix hang/failure when using udt driver with local client
    transfer

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 01 Aug 2017 07:24:52 +0200

globus-ftp-control (7.7-2) unstable; urgency=medium

  * Migrate to dbgsym packages
  * Support DEB_BUILD_OPTIONS=nocheck

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:28 +0200

globus-ftp-control (7.7-1) unstable; urgency=medium

  * GT6 update: Improve forced ordering

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 11 Nov 2016 10:57:41 +0100

globus-ftp-control (7.4-1) unstable; urgency=medium

  * GT6 update: Updates for OpenSSL 1.1.0

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 01 Sep 2016 16:05:21 +0200

globus-ftp-control (7.2-1) unstable; urgency=medium

  * GT6 update (Add buffering to data ordering mode)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Jul 2016 18:08:57 +0200

globus-ftp-control (7.1-1) unstable; urgency=medium

  * GT6 update (Add forced ordering option)
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 15 Jul 2016 11:50:20 +0200

globus-ftp-control (6.10-1) unstable; urgency=medium

  * GT6 update

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 03 May 2016 19:10:36 +0200

globus-ftp-control (6.8-2) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 11:47:38 +0100

globus-ftp-control (6.8-1) unstable; urgency=medium

  * GT6 update (GT-594: enable keepalives)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Oct 2015 11:16:45 +0100

globus-ftp-control (6.7-1) unstable; urgency=medium

  * GT6 update (Fix old-style function definitions, Fix variable scope)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 29 Jul 2015 12:59:25 +0200

globus-ftp-control (6.6-2) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630050)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 19:19:12 +0200

globus-ftp-control (6.6-1) unstable; urgency=medium

  * GT6 update (test fixes, missing return value)
  * Set GLOBUS_HOSTNAME during make check

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 19 Feb 2015 13:26:01 +0100

globus-ftp-control (6.3-1) unstable; urgency=medium

  * GT6 update
  * Drop patches globus-ftp-control-memleak.patch and
    globus-ftp-control-tests-localhost.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 15 Nov 2014 14:11:48 +0100

globus-ftp-control (5.12-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 22:36:12 +0100

globus-ftp-control (5.12-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package
  * Enable verbose tests

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 11:03:43 +0100

globus-ftp-control (5.12-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-ftp-control-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 Oct 2014 19:11:51 +0100

globus-ftp-control (5.11-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Enable checks

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 22:35:16 +0200

globus-ftp-control (4.7-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Drop patch globus-ftp-control-authinfo.patch (fixed upstream)
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 22:20:22 +0100

globus-ftp-control (4.5-3) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:46:42 +0200

globus-ftp-control (4.5-2) unstable; urgency=low

  * Fix modification to wrong authinfo object

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Feb 2013 16:48:21 +0100

globus-ftp-control (4.5-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.4

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 20 Feb 2013 14:12:50 +0100

globus-ftp-control (4.4-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patch globus-ftp-control-deps.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 12:33:05 +0200

globus-ftp-control (4.2-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 19:13:17 +0100

globus-ftp-control (4.2-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patches globus-ftp-control-backcompat.patch,
    globus-ftp-control-doxygen.patch, globus-ftp-control-format.patch and
    globus-ftp-control-type-punned-pointer.patch (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 17:25:44 +0100
globus-ftp-control (2.12-2) unstable; urgency=low

  * Fix backward incompatibility

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 25 Jun 2011 05:21:54 +0200

globus-ftp-control (2.12-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.4
  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 06 Jun 2011 13:40:38 +0200

globus-ftp-control (2.11-3) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616223)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 14:34:37 +0200

globus-ftp-control (2.11-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583058)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:29 +0200

globus-ftp-control (2.11-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 27 Jan 2010 08:37:29 +0100

globus-ftp-control (2.10-6) unstable; urgency=low

  * Fix format errors.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 04 Jun 2009 05:09:58 +0200

globus-ftp-control (2.10-5) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 20:42:22 +0200

globus-ftp-control (2.10-4) unstable; urgency=low

  * Fixing inherited dependency of binary to the libgssapi-error2
    package which has now been removed from the archive.

 -- Steffen Moeller <moeller@debian.org>  Fri, 01 May 2009 10:07:20 +0200

globus-ftp-control (2.10-3) unstable; urgency=low

  * Initial release (Closes: #512969).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:30 +0200

globus-ftp-control (2.10-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-ftp-control (2.10-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 11:19:22 +0100
